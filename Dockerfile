FROM tomcat:9.0

WORKDIR /usr/local/tomcat
RUN chgrp -R 0 /usr/local/tomcat && chmod -R g=u /usr/local/tomcat

COPY geoserver.war ./webapps

EXPOSE 8080

CMD ["catalina.sh", "run"]
